Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: * src/imageformats/doc/src/*
Copyright: 2016-2021 The Qt Company Ltd.
License: LGPL-3 or GPL-2

Files: tests/auto/jp2/* tests/auto/webp/* tests/shared/images/heif/* tests/shared/images/icns/* tests/shared/images/jp2/*
Copyright: 2016-2021 The Qt Company Ltd.
License: GPL-3 with Qt-1.0 exception

Files: src/3rdparty/libwebp/sharpyuv/* src/3rdparty/libwebp/src/*
Copyright: 2010-2021 Google Inc.
License: BSD-3-clause

Files: src/plugins/imageformats/dds/*
Copyright: 2016 Ivan Komissarov
 2016 The Qt Company Ltd.
License: LGPL-3 or GPL-2

Files: src/plugins/imageformats/icns/*
Copyright: 2016 Alex Char
 2016 The Qt Company Ltd.
License: LGPL-3 or GPL-2

Files: src/plugins/imageformats/jp2/*
Copyright: 2016 Petroules Corporation
 2016 The Qt Company Ltd.
License: LGPL-3 or GPL-2

Files: .QT-ENTERPRISE-LICENSE-AGREEMENT
Copyright: treaties, as well as other intellectual property laws and treaties. The Licensed Software is licensed, not sold.
License: LGPL-2.1+

Files: CMakeLists.txt
 configure.cmake
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: LICENSES/BSD-3-Clause.txt
Copyright: no-info-found
License: BSD-3-clause

Files: LICENSES/GFDL-1.3-no-invariants-only.txt
Copyright: 2000-2002, 2007, 2008, Free Software Foundation, Inc. <http://fsf.org/>
License: GFDL-1.3

Files: LICENSES/GPL-2.0-only.txt
Copyright: 1989, 1991, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2

Files: LICENSES/GPL-3.0-only.txt
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3

Files: LICENSES/LGPL-3.0-only.txt
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-3

Files: LICENSES/libtiff.txt
Copyright: 1991-1997, Silicon Graphics, Inc.
 1988-1997, Sam Leffler
License: libtiff

Files: cmake/*
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: debian/*
Copyright: 2021-2025, Patrick Franz <deltaone@debian.org>
License: GPL-2 or LGPL-3

Files: dist/changes-5.7.0
Copyright: no-info-found
License: LGPL-2.1

Files: src/3rdparty/libtiff/*
Copyright: 1991-1997, Silicon Graphics, Inc.
 1988-1997, Sam Leffler
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_hash_set.c
 src/3rdparty/libtiff/libtiff/tif_hash_set.h
Copyright: 2008, 2009, Even Rouault <even dot rouault at spatialys.com>
License: Expat

Files: src/3rdparty/libtiff/libtiff/tif_lerc.c
Copyright: 2018, Even Rouault <even dot rouault at spatialys.com>
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_luv.c
Copyright: 1997, Silicon Graphics, Inc.
 1997, Greg Ward Larson
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_lzma.c
Copyright: 2010, Andrey Kiselev <dron@ak4719.spb.edu>
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_lzw.c
Copyright: 2022, Even Rouault <even dot rouault at spatialys.com>
 1991-1997, Silicon Graphics, Inc.
 1988-1997, Sam Leffler
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_pixarlog.c
Copyright: 1996, Pixar
 1996, 1997, Sam Leffler
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_webp.c
Copyright: 2018, Mapbox
License: libtiff

Files: src/3rdparty/libtiff/libtiff/tif_zstd.c
Copyright: 2017, Planet Labs
License: libtiff

Files: src/3rdparty/libtiff/qt_attribution.json
Copyright: 1988-1997, Sam Leffler 1991-1997, Silicon Graphics, Inc."
License: libtiff

Files: src/3rdparty/libwebp/*
Copyright: 2010, Google Inc.
License: BSD-3-clause

Files: src/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/imageformats/*
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/icns/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/jp2/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/macheif/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/macjp2/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/mng/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/tga/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/tiff/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/wbmp/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: src/plugins/imageformats/webp/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/auto/*
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/auto/jp2/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/auto/webp/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/benchmarks/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/benchmarks/mng/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: tests/benchmarks/tiff/CMakeLists.txt
Copyright: 2022, The Qt Company Ltd.
License: BSD-3-clause

Files: REUSE.toml dist/REUSE.toml
Copyright: 2016-2021 The Qt Company Ltd.
License: LGPL-3 or GPL-2

Files: tests/auto/heif/tst_qheif.cpp tests/auto/mng/tst_qmng.cpp tests/auto/tga/tst_qtga.cpp tests/auto/tiff/tst_qtiff.cpp tests/auto/wbmp/tst_qwbmp.cpp tests/auto/webp/images/kollada_noalpha.webp tests/auto/webp/tst_qwebp.cpp tests/shared/images/dds/A16B16G16R16.dds tests/shared/images/dds/A2B10G10R10.dds tests/shared/images/dds/A2R10G10B10.dds tests/shared/images/dds/A2W10V10U10.dds tests/shared/images/dds/A32B32G32R32F.dds tests/shared/images/dds/A8B8G8R8.dds tests/shared/images/dds/A8R8G8B8.2.dds tests/shared/images/dds/A8R8G8B8.dds tests/shared/images/dds/ATI2.dds tests/shared/images/dds/CxV8U8.dds tests/shared/images/dds/DXT1.dds tests/shared/images/dds/DXT2.dds tests/shared/images/dds/DXT3.dds tests/shared/images/dds/DXT4.dds tests/shared/images/dds/DXT5.dds tests/shared/images/dds/G16R16.dds tests/shared/images/dds/G32R32F.dds tests/shared/images/dds/G8R8_G8B8.dds tests/shared/images/dds/L16.dds tests/shared/images/dds/L6V5U5.dds tests/shared/images/dds/L8.dds tests/shared/images/dds/P8.dds tests/shared/images/dds/Q16W16V16U16.dds tests/shared/images/dds/Q8W8V8U8.dds tests/shared/images/dds/R32F.dds tests/shared/images/dds/R8G8B8.dds tests/shared/images/dds/R8G8_B8G8.dds tests/shared/images/dds/RXGB.dds tests/shared/images/dds/UYVY.dds tests/shared/images/dds/V16U16.dds tests/shared/images/dds/X8B8G8R8.dds tests/shared/images/dds/X8L8V8U8.dds tests/shared/images/dds/X8R8G8B8.dds tests/shared/images/dds/YUY2.dds tests/shared/images/dds/mipmaps.dds tests/shared/images/heif/col320x480.heic tests/shared/images/icns/test-32bit.icns tests/shared/images/icns/test-jp2.icns tests/shared/images/icns/test-png.icns tests/shared/images/icns/test-variants.icns tests/shared/images/mng/animation.mng tests/shared/images/mng/ball.mng tests/shared/images/mng/dutch.mng tests/shared/images/mng/fire.mng tests/shared/images/tiff/16bpc.tiff tests/shared/images/tiff/big_16bpc.tiff tests/shared/images/tiff/gray16.tiff tests/shared/images/tiff/indexed_orientation_1.tiff tests/shared/images/tiff/indexed_orientation_2.tiff tests/shared/images/tiff/indexed_orientation_3.tiff tests/shared/images/tiff/indexed_orientation_4.tiff tests/shared/images/tiff/indexed_orientation_5.tiff tests/shared/images/tiff/indexed_orientation_6.tiff tests/shared/images/tiff/indexed_orientation_7.tiff tests/shared/images/tiff/indexed_orientation_8.tiff tests/shared/images/tiff/multipage.tiff tests/shared/images/tiff/oddsize_grayscale.tiff tests/shared/images/tiff/oddsize_mono.tiff tests/shared/images/tiff/original_indexed.tiff tests/shared/images/tiff/teapot.tiff tests/shared/images/tiff/teapot_cmyk.tiff tests/shared/images/tiff/tiled_indexed.tiff tests/shared/images/tiff/tiled_oddsize_grayscale.tiff tests/shared/images/tiff/tiled_oddsize_mono.tiff tests/shared/images/wbmp/qt-logo-small.wbmp
Copyright: 2016-2021 The Qt Company Ltd.
License: GPL-3 with Qt-1.0 exception

Files: src/3rdparty/libtiff/REUSE.toml src/3rdparty/libtiff/libtiff/tif_ojpeg.c src/3rdparty/libtiff/libtiff/tiffvers.h
Copyright: 1988-1997 Sam Leffler
 1991-1997 Silicon Graphics, Inc.
 2002-2010 Andrey Kiselev <dron@ak4719.spb.edu>
License: BSD-libtiff

Files: src/3rdparty/libwebp/REUSE.toml src/3rdparty/libwebp/qt_attribution.json
Copyright: 2010-2021 Google Inc.
License: BSD-3-clause

Files: tests/auto/dds/tst_qdds.cpp
Copyright: 2016 Ivan Komissarov
 2016 The Qt Company Ltd.
License: GPL-3 with Qt-1.0 exception

Files: tests/auto/icns/tst_qicns.cpp
Copyright: 2016 Alex Char
 2016 The Qt Company Ltd.
License: GPL-3 with Qt-1.0 exception
